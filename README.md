
(1) La base de donnees
    Pour renseigner les identifants de connexion a la base de donnee, aller dans le fichier ./.env et decommenter la ligne 33 puis commenter la ligne 32.
    Pour configurer la base de donnees : 
    	php bin/console doctrine:database:create
    	php bin/console make:migration
    	php bin/console doctrine:migrations:migrate

(2) Executer
    Pour le faire, lancer la commande suivante dans le repertoire du projet
        php -S 127.0.0.1:8000 -t public

(2) Route particuliere:
    Toutes les routes sont definies dans ./src/Controller a l'acception de la route de nom unknownRoute qui est definie dans ./config/routes.yaml et dont la fonction execute est definie dans ./Controller/MainController. Cette route matche les routes inconnues.

(3) Connection des utilisateurs
    Elle est geree avec des sessions. Nous stockons dans la session une cle "userid" dont la valeur est l'identifiant dans la base de donnees de l'utilisateur connecte. La fonction MainController::checkConnection permet de verifier si un utilisateur est connecte et retourne cet identifiant le cas echeant.

(4) Les exercices
    Chaque exercice est represente par un nom et une description et est assigne a un cours. Les exercices contiennent aussi des lignes qui represente la solution a l'exercice. Chaque ligne etant un triplet texte-indentation-numero associe a un exercice, nous pouvons aisement verifier que l'indentation du code est comme le veut le createur de l'exercice. Cependant, ce systeme ne permet pas du tout de flexibiliter pour les reponses aux exercices etant donne que chaque solution proposee n'est correcte que si elle matche parfaitement celle propose par le "Professeur".

(5) Les identifiants utilisateurs
    Les mots de passe utilisateurs sont contraints a avoir une taille minimum de 8 caracteres et sont encryptes et sales avant leur ecriture dans la base de donnees. les nom d'utilisateur aux sont uniques, une verification est faite a chaque creation de nouvel utilisateur.