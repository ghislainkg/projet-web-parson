
var navButtons = document.querySelectorAll(".nav-tabs .nav-link")
navButtons.forEach(button => {
    button.addEventListener("click", function () {
        var content = document.getElementById("coursTab")
        var target = content.querySelector("#"+this.id)
        var active = content.querySelector(".active")

        active.classList.remove("show")
        active.classList.remove("active")
        var activeTab = document.querySelector(".nav-tabs .active")
        activeTab.classList.remove("active")

        target.classList.add("active")
        target.classList.add("show")
        this.classList.add("active")
    })
});
