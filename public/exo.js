
let movingCode;

function updateListeners() {
    let lineContainer = document.querySelectorAll(".container-line")[0];
    let contains =lineContainer.querySelectorAll(".line");
    for (let i = 0; i < contains.length; i++) {
        const line = contains[i];
        addDragDropListeners(line);
    }

    let codes = document.querySelectorAll(".code");
    for (let i = 0; i < codes.length; i++) {
        const code = codes[i];
        code.addEventListener("dragstart", (event) => {
            console.log("Drag start")
            movingCode = event.target;
        })
        code.addEventListener("dragend", (event) => {
            console.log("drag end")
            movingCode = undefined;
        });
    }

    let colones = document.querySelectorAll(".colone");
    for (let i = 0; i < colones.length; i++) {
        const colone = colones[i];
        addDragDropListeners(colone);
    }
}
function addDragDropListeners(contain) {
    addDragDropOthersListeners(contain);
    contain.addEventListener("drop", (event) => {
        console.log("Drop")
        if(isLineOk(event.target)) {
            event.target.append(movingCode);
        }
    }, {capture : true});
}
function addDragDropOthersListeners(target) {
    target.addEventListener("dragover", (event) => {
        event.preventDefault();
        console.log("over")
    });
    target.addEventListener("dragenter", (event) => {
        event.preventDefault();
        console.log("drag enter");
    });
    target.addEventListener("dragleave", (event) => {
        event.preventDefault();
        console.log("Drag leave");
    });
}
function isLineOk(colone) {
    return isLineEmpty(colone.parentNode) && (colone.classList.contains("colone") || colone.classList.contains("line"));
}
function isLineEmpty(lineDiv) {
    let colones = lineDiv.querySelectorAll(".colone");
    for (let i = 0; i < colones.length; i++) {
        const colone = colones[i];
        if(colone.childElementCount > 0) {
            return false;
        }
    }
    return true;
}

function addCodeLine(line) {
    let linesContainer = document.querySelectorAll(".container-line")[0];
    let lineDiv = document.createElement("div");
    lineDiv.classList.add("line");

    let code = document.createElement("div");
    code.classList.add("code");
    code.classList.add("badge-primary");
    code.classList.add("badge");
    code.setAttribute("draggable", "true");
    code.setAttribute("z-index", 99);
    code.innerText = line.contenu;

    lineDiv.append(code);
    linesContainer.append(lineDiv);
    updateListeners();
}

function addEmptyAnswerLine(coloneCount) {
    let linesContainer = document.querySelectorAll(".container-code")[0];
    let lineDiv = document.createElement("div");
    lineDiv.classList.add("line-answer");
    linesContainer.append(lineDiv);

    for (let i = 0; i < coloneCount; i++) {
        let colone = document.createElement("div");
        colone.classList.add("colone");
        lineDiv.append(colone);
    }
    updateListeners();
}

function setAnswerLine(line) {
    let linesContainer = document.querySelectorAll(".container-code")[0];
    let lineDivs = linesContainer.querySelectorAll(".line-answer");
    let lineDiv = lineDivs[line.numero];
    let colones = lineDiv.querySelectorAll(".colone");
    let colone = colones[line.indent];

    let code = document.createElement("div");
    code.classList.add("code");
    code.setAttribute("draggable", "true");
    code.innerText = line.contenu;

    colone.append(code);
    updateListeners();
}

function getResults() {
    let codeLignes = [];
    let codeContainer = document.querySelectorAll(".container-code")[0];
    let lines = codeContainer.querySelectorAll(".line-answer");
    for (let i = 0; i < lines.length; i++) {
        const line = lines[i];
        let res = getLineResult(line, i);
        if(res != undefined) {
            codeLignes.push(res);
        }
    }
    console.log(codeLignes)
    return codeLignes;
}
function getLineResult(line, index) {
    let codeLigne = {};
    let colones = line.querySelectorAll(".colone");
    for (let i = 0; i < colones.length; i++) {
        const colone = colones[i];
        let coloneContents = colone.querySelectorAll(".code");
        if(coloneContents.length > 0) {
            codeLigne.contenu = coloneContents[0].innerText;
            codeLigne.numero = index;
            codeLigne.indent = i;
            return codeLigne;
        }
    }
    return undefined;
}

var send_exo = function (_exoid, target) {
    var sss = getResults();
    $.ajax({
        type : "POST",
        url : target,
        data : {
            exoid : _exoid,
            coursid : _,
            lignes : sss
        }
    })
    .done((res) => {
        window.alert(res);
    })
}

function request_lignes(_exoid, onLignes) {
    $.ajax({
        type : "POST",
        url : "/request_lignes",
        data : {
            exoid : _exoid
        }
    })
    .done((res) => {
        onLignes(res.lignes);
    })
}

