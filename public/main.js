
var closeBtns = document.querySelectorAll(".close")
for (let i = 0; i < closeBtns.length; i++) {
    const btn = closeBtns[i];
    btn.addEventListener("click", (event) => {
        console.log("Click")
        var parent = event.target.parentNode
        parent.parentNode.removeChild(parent)
    })
}

var menu = document.querySelector("#menu");
var menuBtn = document.querySelector("#menubutton");
menuBtn.addEventListener("click", (event) => {
    if(menu.style.display == "none") {
        menu.style.display = "block";
    }
    else {
        menu.style.display = "none";
    }
})
