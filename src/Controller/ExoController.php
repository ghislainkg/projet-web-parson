<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Controller\MainController;

use App\Entity\Cours;
use App\Entity\Exercice;
use App\Entity\Ligne;
use App\Entity\Resultat;
use App\Entity\Solution;
use App\Entity\User;

class ExoController extends AbstractController
{
    /**
     * @Route("/show_exo/{exo_id}", name="show_exo")
     */
    // Afficher l'exercice d'id $exo_id
    public function show_exo(Request $request, $exo_id) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $renderArgs = [];

        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        $renderArgs["user"] = $user;

        $repository = $this->getDoctrine()->getRepository(Exercice::class);
        $exo = $repository->getById(intval($exo_id));
        $renderArgs["exo"] = $exo; // L'exercice a afficher

        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user->getType() == User::USER_TYPE_ENSEIGNANT) {
            $renderArgs["enseignant"] = ""; // On precise que c'est un enseignant
        }
        $renderArgs["which"] = 1; // On dit bien a la page que l'on affiche un exercice (ou plusieurs)

        return $this->render('exo/exo_show.html.twig', $renderArgs);
    }


    /**
     * @Route("/try_exo/{exo_id}", name="try_exo")
     */
    // Envois sur la page d'essaie d'exercice
    public function try_exo(Request $request, $exo_id) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $renderArgs = [];
        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        $renderArgs["user"] = $user;
        
        $repository = $this->getDoctrine()->getRepository(Exercice::class);
        $exo = $repository->getById(intval($exo_id));
        $renderArgs["exo"] = $exo;
        $renderArgs["which"] = 1; // exo

        return $this->render('exo/exo_try.html.twig', $renderArgs);
    }

    /**
     * @Route("/request_lignes", name="request_lignes")
     */
    public function handle_lignes_request(Request $request) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $exoid = intval($request->request->get("exoid"));
        $exo = $this->getDoctrine()->getRepository(Exercice::class)->getById($exoid);

        $lignes = [];
        for ($i=0; $i < count($exo->getLignes()); $i++) { 
            $lignes[] = [
                "contenu" => $exo->getLignes()[$i]->getContenu(),
                "indent" => $exo->getLignes()[$i]->getIndentation()
            ];
        }

        return $this->json([
            "lignes" => $lignes
        ]);
    }

    /**
     * @Route("/try_exo_handling", name="try_exo_handling")
     */
    // L'utilisateur a terminer l'exercice, on recoit sa reponse
    public function try_exo_handling(Request $request) {
        // On recupere l'utilisateur pour pouvoir enregistrer les resultat de l'essaie
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }
        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }

        // On recoit les lignes et l'id de l'exercice produit par l'utilisateur
        $lignes = $request->request->get("lignes");
        if(!is_array($lignes)) {
            // Si on n'a pas un tableau, alors c'est rate
            return new Response("fail", 200);
        }
        $exoid = intval($request->request->get("exoid"));
        // On recupere l'exercice qui a ete essaye
        $repoExo = $this->getDoctrine()->getRepository(Exercice::class);
        $exo = $repoExo->getById($exoid);

        // On compte le nombre de lignes valides
        $validLigneCount = 0;
        $l = $exo->getLignes();
        $index=0;
        for ($index=0; $index < count($lignes); $index++) { 
            for ($j=0; $j < count($l); $j++) { 
                if($l[$j]->getContenu() == $lignes[$index]["contenu"] && 
                $l[$j]->getIndentation() == $lignes[$index]["indent"] &&
                $l[$j]->getLineNumber() == $lignes[$index]["numero"]) {
                    $validLigneCount += 1;
                }
            }
        }
 
        $manager = $this->getDoctrine()->getManager();
        if($validLigneCount == count($lignes) && $validLigneCount == count($l)) {
            // Si on a le bon nombre de lignes valides, On dit que tout est bon
            // apres avoir enregistrer cette victoire face a la programation
            $resultat = new Resultat();
            $resultat->setExercice($exo);
            $resultat->setResultat(100);
            $resultat->setEtudiant($user);
            $manager->persist($resultat);
            $manager->flush();
            return new Response("success", 200);
        }
        else {
            // Sinon, on enregistrer cette defaite :(
            $resultat = new Resultat();
            $resultat->setExercice($exo);
            $resultat->setResultat(25);
            $resultat->setEtudiant($user);
            $manager->persist($resultat);
            $manager->flush();
            return new Response("fail", 200);
        }
    }


    /**
     * @Route("/exo", name="exo")
     */
    // Affiche tous les exercices
    public function liste_exercices(Request $request) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $renderArgs = [];
        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        $renderArgs["user"] = $user;

        $repository = $this->getDoctrine()->getRepository(Exercice::class);
        $exos = $repository->getAll();
        $renderArgs["exo_list"] = $exos;
        $renderArgs["which"] = 1; // Exo

        return $this->render('exo/exo_list.html.twig', $renderArgs);
    }

    /**
     * @Route("/exo_edit/{cours_id}", name="exo_edit")
     */
    // Edition d'un nouvel exercice pour le cours d'id $cours_id
    public function edit_exercice(Request $request, $cours_id) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $renderArgs = [];

        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        // On verifie que l'utilisateur est un enseignant
        if($user->getType() != User::USER_TYPE_ENSEIGNANT) {
            MainController::setMessage($request, "Desole mais ... vous n'avez pas le droit de creer d'exercices");
            return $this->redirectToRoute("home");
        }
        $renderArgs["user"] = $user;
        
        // C'est un enseignant. On recupere le cours du future exercice
        $repository = $this->getDoctrine()->getRepository(Cours::class);
        $cours = $repository->getById(intval($cours_id));
        $renderArgs["cours"] = $cours;
        $renderArgs["which"] = 1; // exos
        // Et en route vers l'edition de ce cours !!!
        return $this->render('exo/exo_edit.html.twig', $renderArgs);
    }
    /**
     * @Route("/exo_edit_handling", name="exo_edit_handling")
     */
    // Ouf ... L'utilisateur a termine l'edition de l'exercice. Voyon voir ce qu'il a fait
    public function edit_exercice_handling(Request $request) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;}

        // On recupere toutes les informations
        $lignes = $request->request->get("lignes");
        if(!is_array($lignes)) {
            // Si on n'a pas un tableau, alors c'est rate
            return new Response("fail", 200);
        }
        $nom = $request->request->get("nom");
        $description = $request->request->get("description");
        $coursid = intval($request->request->get("coursid"));

        // Creons l'exercice
        $coursRepo = $this->getDoctrine()->getRepository(Cours::class);
        $cours = $coursRepo->getById($coursid);
        $exo = new Exercice();
        $exo->setNom($nom)
            ->setDescription($description)
            ->setCours($cours);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($exo);
        $manager->flush();

        // Creons les lignes
        for ($i=0; $i < count($lignes); $i++) { 
            $ligne = $lignes[$i];
            $trueLigne = new Ligne();
            $trueLigne->setContenu($ligne["contenu"]);
            $trueLigne->setIndentation($ligne["indent"]);
            $trueLigne->setExercice($exo);
            $trueLigne->setLineNumber($ligne["numero"]);
            $manager->persist($trueLigne);
        }
        $manager->flush();

        // Cette url est envoyer en reponse a la requete ajax.
        // Le javascript vas se charger de la suivre
        $nextUrl = $this->generateUrl("show_cours", ["cours_id" => $coursid], true);
        return new Response($nextUrl, 200);
    }


    /**
     * @Route("/exo_update/{exo_id}", name="exo_update")
     */
    // Modification d'un exercice pour le cours d'id $cours_id
    public function update_exercice(Request $request, $exo_id) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }
        
        $renderArgs = [];

        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        // On verifie que l'utilisateur est un enseignant
        if($user->getType() != User::USER_TYPE_ENSEIGNANT) {
            MainController::setMessage($request, "Desole mais ... vous n'avez pas le droit de creer d'exercices");
            return $this->redirectToRoute("home");
        }
        $renderArgs["user"] = $user;

        // C'est un enseignant. On recupere l'exercice a modifier
        $exo = $this->getDoctrine()->getRepository(Exercice::class)->getById($exo_id);
        $renderArgs["cours"] = $exo->getCours();
        $renderArgs["exo"] = $exo;
        $renderArgs["which"] = 1; // exercices
        // Et en route vers l'edition de ce cours !!!
        return $this->render('exo/exo_edit.html.twig', $renderArgs);
    }
    /**
     * @Route("/exo_update_handling", name="exo_update_handling")
     */
    // Ouf ... L'utilisateur a termine l'edition de l'exercice. Voyon voir ce qu'il a fait
    public function update_exercice_handling(Request $request) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        // On recupere toutes les informations
        $lignes = $request->request->get("lignes");
        if(!is_array($lignes)) {
            // Si on n'a pas un tableau, alors c'est rate
            return new Response("fail", 200);
        }
        $nom = $request->request->get("nom");
        $description = $request->request->get("description");

        // recuperons et modifions l'exercice
        $exo_id = $request->request->get("exoid");
        $exo = $this->getDoctrine()->getRepository(Exercice::class)->getById($exo_id);
        $exo->setNom($nom)
            ->setDescription($description);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($exo);
        $manager->flush();

        // Supprimons les anciennes lignes
        $oldLignes = $exo->getLignes();
        for ($i=0; $i < count($oldLignes); $i++) { 
            $manager->remove($oldLignes[$i]);
        }
        $manager->flush();

        // Creons les nouvelles lignes
        for ($i=0; $i < count($lignes); $i++) { 
            $ligne = $lignes[$i];
            $trueLigne = new Ligne();
            $trueLigne->setContenu($ligne["contenu"]);
            $trueLigne->setIndentation(intval($ligne["indent"]));
            $trueLigne->setExercice($exo);
            $trueLigne->setLineNumber(intval($ligne["numero"]));
            $manager->persist($trueLigne);
        }
        $manager->flush();

        return new Response("ok", 200);
    }

    /**
     * @Route("/delete_exercice_handling/{exo_id}", name="delete_exercice_handling")
     */
    public function delete_exercice_handling(Request $request, $exo_id) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $exo = $this->getDoctrine()->getRepository(Exercice::class)->getById(intval($exo_id));
        $this->getDoctrine()->getManager()->remove($exo);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute("exo");
    }
}
