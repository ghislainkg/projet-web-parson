<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Controller\MainController;

use App\Entity\Cours;
use App\Entity\Exercice;
use App\Entity\Ligne;
use App\Entity\Resultat;
use App\Entity\Solution;
use App\Entity\User;

class CoursController extends AbstractController
{
    /**
     * @Route("/cours", name="cours")
     */
    // Affiche la liste des cours dont l'utilisateur a essayer au moins un exercice
    public function liste_cours(Request $request) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $renderArgs = [];

        // Il pourrais y avoir un message a afficher
        $message = MainController::getMessage($request);
        MainController::removeMessage($request);
        $renderArgs["message"] = $message;
        
        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        if($user->getType() == User::USER_TYPE_ENSEIGNANT) {
            // Un end=seignant peut voir tous les cours
            $cours = $this->getDoctrine()->getRepository(Cours::class)->getAll();
        }
        else {
            $cours = $user->getCours();
        }
        $renderArgs["cours_list"] = $cours;

        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        $renderArgs["user"] = $user;

        $renderArgs["which"] = 0; // cours

        return $this->render('cours/cours_list.html.twig', $renderArgs);
    }


    /**
     * @Route("/show_cours/{cours_id}", name="show_cours")
     */
    // Affiche le cours d'id $cours_id
    public function show_cours(Request $request, $cours_id) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $renderArgs = [];

        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        $renderArgs["user"] = $user;

        $coursRepo = $this->getDoctrine()->getRepository(Cours::class);
        $cours = $coursRepo->getById(intval($cours_id));
        $renderArgs["cours"] = $cours;
        $renderArgs["exo_list"] = $cours->getExercices();
        $renderArgs["user_list"] = $cours->getEtudiants();

        $renderArgs["which"] = 0; // cours

        return $this->render('cours/cours_show.html.twig', $renderArgs);
    }

    /**
     * @Route("/edit_cours", name="edit_cours")
     */
    // Envoie sur l'editeur de cours
    public function edit_cours(Request $request) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $renderArgs = [];
        
        // On verifie que l'utilisateur est un enseignant
        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        if($user->getType() != User::USER_TYPE_ENSEIGNANT) {
            MainController::setMessage($request, "Vous n'avez pas le droit de creer de cours");
            return $this->redirectToRoute("home");
        }
        $renderArgs["user"] = $user;
        $renderArgs["which"] = 0; // cours
        // C'est un enseignant donc fine ... Welcome
        return $this->render('cours/cours_edit.html.twig', $renderArgs);
    }
    /**
     * @Route("/edit_cours_handling", name="edit_cours_handling")
     */
    // Une fois l'edition du cours terminer, on recoit les information transmise par l'utilisateur
    public function edit_cours_handling(Request $request) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        // On recupere le nom et la description du cours
        $coursname = $request->request->get("nom");
        $coursdescription = $request->request->get("description");
        // On cree le cours
        $cours = new Cours();
        $cours->setNom($coursname);
        $cours->setDescription($coursdescription);
        // On inscrit l'utilisateur
        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        $cours->addEtudiant($user);
        $user->addCour($cours);
        // On enregistre le cours dans la base de donnees
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($cours);
        $manager->persist($user);
        $manager->flush();
        // On vas vers home (Liste de tous les cours)
        return $this->redirectToRoute("home");
    }


    /**
     * @Route("/update_cours/{cours_id}", name="update_cours")
     */
    // Envoie sur l'editeur de cours pour la modification d'un cours
    public function update_cours($cours_id, Request $request) {
        // biensur si on n'est pas connecter, on retourne vers la connection
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $renderArgs = [];

        // On verifie que l'utilisateur est un enseignant
        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        if($user->getType() != User::USER_TYPE_ENSEIGNANT) {
            MainController::setMessage($request, "Vous n'avez pas le droit de creer de cours");
            return $this->redirectToRoute("home");
        }
        $renderArgs["user"] = $user;
        $cours = $this->getDoctrine()->getRepository(Cours::class)->getById(intval($cours_id));
        $renderArgs["cours"] = $cours;
        $renderArgs["which"] = 0; // cours
        // C'est un enseignant donc fine ... Welcome
        return $this->render('cours/cours_edit.html.twig', $renderArgs);
    }
    /**
     * @Route("/update_cours_handling/{cours_id}", name="update_cours_handling")
     */
    // Une fois la modification du cours terminer, on recoit les information transmise par l'utilisateur
    public function update_cours_handling($cours_id, Request $request) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        // On recupere le nom et la description du cours
        $coursname = $request->request->get("nom");
        $coursdescription = $request->request->get("description");
        // On recupere le cours et on le met a jour
        $cours = $this->getDoctrine()->getRepository(Cours::class)->getById(intval($cours_id));
        $cours->setNom($coursname);
        $cours->setDescription($coursdescription);
        // On met a jours le cours dans la base de donnees
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($cours);
        $manager->flush();
        // On vas vers home (Liste de tous les cours)
        return $this->redirectToRoute("home");
    }

    /**
     * @Route("/delete_cours_handling/{cours_id}", name="delete_cours_handling")
     */
    public function delete_cours_handling(Request $request, $cours_id) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $cours = $this->getDoctrine()->getRepository(Cours::class)->getById(intval($cours_id));
        // Supprimons le cours
        foreach ($cours->getExercices() as $exo) {
            $this->getDoctrine()->getManager()->remove($exo);
            foreach ($exo->getResultats() as $resultat) {
                $this->getDoctrine()->getManager()->remove($resultat);
            }
            foreach ($exo->getLignes() as $ligne) {
                $this->getDoctrine()->getManager()->remove($ligne);
            }
        }
        $this->getDoctrine()->getManager()->remove($cours);
        $this->getDoctrine()->getManager()->flush();

        // On redirige vers la liste des cours de l'utilisateur
        return $this->redirectToRoute("cours");
    }

    /**
     * @Route("/inscription_cours_handling/{cours_id}", name="inscription_cours_handling")
     */
    public function inscription_cours_handling(LoggerInterface $logger, Request $request, $cours_id) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $cours = $this->getDoctrine()->getRepository(Cours::class)->getById(intval($cours_id));
        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        $cours->addEtudiant($user);
        $user->addCour($cours);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($cours);
        $manager->persist($user);
        $manager->flush();

        return $this->redirectToRoute("show_cours", ["cours_id" => $cours_id]);
    }

    /**
     * @Route("/desinscription_cours_handling/{cours_id}", name="desinscription_cours_handling")
     */
    public function desinscription_cours_handling(LoggerInterface $logger, Request $request, $cours_id) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $cours = $this->getDoctrine()->getRepository(Cours::class)->getById(intval($cours_id));
        $user = $this->getDoctrine()->getRepository(User::class)->getById(intval($userid));
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        $cours->removeEtudiant($user);
        $user->removeCour($cours);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($cours);
        $manager->persist($user);
        $manager->flush();

        return $this->redirectToRoute("show_cours", ["cours_id" => $cours_id]);
    }

}
