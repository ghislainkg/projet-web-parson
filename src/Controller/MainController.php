<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Cours;
use App\Entity\Exercice;
use App\Entity\Ligne;
use App\Entity\Resultat;
use App\Entity\Solution;
use App\Entity\User;

class MainController extends AbstractController
{
    // Verifi si un utilisateur est conmnecte dans la session de la requette
    // Retourne l'id le cas echean et redirige vers la page de connection dans le cas contraire
    public static function checkConnection(Request $request, $controller) {
        $session = $request->getSession();
        $id = $session->get("userid", null);
        if($id == null) {
            return $controller->redirectToRoute("connect_user");
        }
        else {
            return $id;
        }
    }
    // Retourne la variable de session "message" contenu dans la requette
    public static function getMessage(Request $request) {
        $session = $request->getSession();
        return $session->get("message");        
    }
    // Fixe la valeur de la variable de session "message" a $message
    public static function setMessage(Request $request, $message) {
        $session = $request->getSession();
        return $session->set("message", $message);
    }
    // Supprime la variable de session "message"
    public static function removeMessage(Request $request) {
        $session = $request->getSession();
        return $session->remove("message");        
    }
    
    /**
     * @Route("/", name="home")
     */
    // Affiche la liste de tous les cours
    public function home(Request $request) {
        $userid = $this->checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $renderArgs = [];
        $user = $this->getDoctrine()->getRepository(User::class)->getById($userid);
        if($user == null) {
            return $this->redirectToRoute("connect_user");
        }
        $renderArgs["user"] = $user;

        // On recupere tous les cours
        $repository = $this->getDoctrine()->getRepository(Cours::class);
        $cours = $repository->getAll();
        $renderArgs["cours_list"] = $cours;

        $message = $this->getMessage($request);
        $this->removeMessage($request);
        $renderArgs["message"] = $message;

        $renderArgs["which"] = 0; // cours
        // Il pourrais y avoir un message a afficher
        return $this->render('cours/cours_list.html.twig', $renderArgs);
    }


    /**
     * @Route("/results", name="results")
     */
    // Affiche la liste de tous les cours
    public function results(Request $request) {
        $userid = $this->checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }
 
        $renderArgs = [];
        $user = $this->getDoctrine()->getRepository(User::class)->getById($userid);
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        $renderArgs["user"] = $user;
        
        if($user->getType() == User::USER_TYPE_ETUDIANT) {
            MainController::setMessage($request, "Vous n'etes pas un enseignant");
            return $this->redirectToRoute("home");
        }

        // On recupere tous les cours
        $repository = $this->getDoctrine()->getRepository(Cours::class);
        $cours = $repository->getAll();
        $renderArgs["cours_list"] = $cours;

        $message = $this->getMessage($request);
        $this->removeMessage($request);
        $renderArgs["message"] = $message;

        $renderArgs["which"] = 2;
        // Il pourrais y avoir un message a afficher
        return $this->render('user/result_by_cours.html.twig', $renderArgs);
    }

    /**
     * @Route("/resultsetudiants", name="resultsetudiants")
     */
    // Affiche les resultats de tous les etudiants
    public function resultsetudiants(Request $request) {
        $userid = $this->checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $renderArgs = [];
        $user = $this->getDoctrine()->getRepository(User::class)->getById($userid);
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        $renderArgs["user"] = $user;

        // On recupere tous les utilisateurs
        $repository = $this->getDoctrine()->getRepository(User::class);
        $usersAux = $repository->getAll();
        // Faisons le tri de etudiants
        $etudiants = [];
        foreach ($usersAux as $userAux) {
            if($userAux->getType() == User::USER_TYPE_ETUDIANT) {
                $etudiants[] = $userAux;
            }
        }
        $renderArgs["user_list"] = $etudiants;

        $message = $this->getMessage($request);
        $this->removeMessage($request);
        $renderArgs["message"] = $message;

        $renderArgs["which"] = 2;
        // Il pourrais y avoir un message a afficher
        return $this->render('user/result_by_user.html.twig', $renderArgs);
    }

    // Pour les routes inconnues
    public function unknownRoute(Request $request) {
        MainController::setMessage($request, "L'url a laquelle vous vouliez acceder n'existe pas");
        return $this->redirectToRoute("home");
    }
}
