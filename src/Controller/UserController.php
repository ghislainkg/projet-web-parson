<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Controller\MainController;

use App\Entity\User;

class UserController extends AbstractController
{
    /**
     * @Route("/compte", name="compte")
     */
    // L'affichage des informations de l'utilisateur
    public function compte(Request $request) {
        // On verifi que l'utilisateur est bien connecter,
        // sinon il sera rediriger vers la page de connection
        $id = MainController::checkConnection($request, $this);
        if(!is_int($id)) {
            return $id;
        }

        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepo->getById(intval($id));

        if($user == null) {
            return $this->redirectToRoute("home");
        }

        // On recupere le message a afficher
        $message = MainController::getMessage($request);
        MainController::removeMessage($request);
        return $this->render('user/user_show.html.twig', [
            "message" => $message, // Ce message est afficher s'il y en a vraiment un
            "user" => $user,
            "which" => 3
        ]);
    }


    /**
     * @Route("/update_user", name="update_user")
     */
    // L'utilisateur a afficher ses informations, s'il a demande des modification, 
    // C'est ici que ca se passe
    public function update_user(Request $request) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        // On recupere les nouveaux nom et mot de passe
        $nom = $request->request->get("nom");
        $mdp = $request->request->get("mdp");
        // On verifi si l'utilisateur exeste belle et bien
        $manager = $this->getDoctrine()->getManager();
        $user = $manager->getRepository(User::class)->getById(intval($userid)); // on reupere l'utilisateur
        if($user) {
            // S'il existe, on met tous a jour
            $user->setNom($nom);
            $user->setMdp($mdp);
            $manager->flush();

            MainController::setMessage($request, "Informations mise a jour");
            return $this->redirectToRoute("compte");
        }
        else {
            // Sinon, on renvoi a la page de creation d'utilisateur avec un message
            MainController::setMessage($request, "Utilisateur inconnue");
            return $this->redirectToRoute("new_user");
        }
    }


    /**
     * @Route("/new_user", name="new_user")
     */
    // Un nouvel utilisateur vas naitre.
    public function new_user(Request $request) {

        // On recupere un message (On ne sait pas de quoi il s'agit ... 
        // de toute facon on ne fait que notre boulot)
        $message = MainController::getMessage($request);
        MainController::removeMessage($request);
        return $this->render('user/user_new.html.twig', [
            "message" => $message, // La page affiche le message s'il n'est pas vide
            "which" => 3
        ]);
    }
    /**
     * @Route("/new_user_handling", name="new_user_handling")
     */
    // On recoit les informations du nouvel utilisateur
    public function new_user_handling(Request $request) {
        // On recupere ces informations
        $nom = $request->request->get("nom");
        $mdp = $request->request->get("mdp");
        $type = $request->request->get("type");
        // On verifi que le nom d'utilisateur n'est pas deja pris
        if(is_string($nom)) {
            $userRepo = $this->getDoctrine()->getRepository(User::class);
            $otheruser = $userRepo->getUser($nom);
            if($otheruser != null) {
                // Un utilisateur existe avec ce nom
                MainController::setMessage($request, "Ce nom d'utilisateur est deja pris");
                return $this->redirectToRoute("new_user");
            }
        }
        else {
            MainController::setMessage($request, "Une erreur s'est produite");
            return $this->redirectToRoute("new_user");
        }
        // On verifi la taille du mot de passe
        if(is_string($mdp)) {
            if(strlen($mdp) < 8) {
                MainController::setMessage($request, "Votre mot de passe doit avoir au mininum 8 caracteres");
                return $this->redirectToRoute("new_user");
            }
        }
        else {
            MainController::setMessage($request, "Une erreur s'est produite");
            return $this->redirectToRoute("new_user");
        }
        // On chiffre le mot de passe
        $mdp = crypt($mdp, "SALT");
        // On cree l'utilisateur
        $user = new User();
        $user->setNom($nom);
        $user->setMdp($mdp);
        $user->setType($type);
        // On l'enregistre dans la base de donnees
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();
        // Et on vas a la page de connection
        return $this->redirectToRoute("connect_user");
    }


    /**
     * @Route("/connect_user", name="connect_user")
     */
    // Page de connect
    public function connect_user(Request $request) {
        // On recupere un message
        // (peut etre on est a la seconde tentative et on a un petit message)
        $message = MainController::getMessage($request);
        MainController::removeMessage($request);
        return $this->render('user/user_connect.html.twig', [
            "message" => $message, // La page affiche le message s'il n'est pas vide
            "which" => 3
        ]);
    }
    /**
     * @Route("/connection", name="connection")
     */
    // L'utilisateur a entrer ses identifiants
    public function connect_handling(Request $request) {
        // On les recupere
        $nom = $request->request->get("nom");
        $mdp = $request->request->get("mdp");
        // On cherche l'utilisateur correspondant
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepo->getUser($nom);

        if(! hash_equals($user->getMdp(), crypt($mdp, "SALT"))) {
            MainController::setMessage($request, "Mauvais mot de passe");
            return $this->redirectToRoute("connect_user");
        }

        // On verifi s'il exeste bien
        if($user == null) {
            // Si non, on retourne sur la page de connection, (avec un petit message)
            MainController::setMessage($request, "Identifiants inconus");
            return $this->redirectToRoute("connect_user");
        }
        else {
            // Si oui, on cree la session de connection de l'utilisateur
            $session = $request->getSession();
            $session->set("userid", $user->getId());
            // On envoie un petit message et on vas vers "home"
            MainController::setMessage($request, "Vous etes connecte");
            return $this->redirectToRoute("home", [
                'which' => 0 // cours
            ]);
        }
    }


    /**
     * @Route("/disconnect", name="disconnect")
     */
    // L'utilisateur demande a se deconnecter
    public function disconnect_handling(Request $request) {
        // On ferme sa session et on le redirige vers home
        $request->getSession()->remove("userid");
        return $this->redirectToRoute("home");
    }

    /**
     * @Route("/delete_user", name="delete_user")
     */
    public function delete_user(Request $request) {
        $userid = MainController::checkConnection($request, $this);
        if(!is_int($userid)) {
            return $userid;
        }

        $user = $this->getDoctrine()->getRepository(User::class)->getById($userid);
        if($user == null) {
            return $this->redirectToRoute("home");
        }
        foreach ($user->getResultats() as $resultat) {
            $this->getDoctrine()->getManager()->remove($resultat);
        }
        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();

        MainController::setMessage($request, "Votre compte a bien ete supprime");
        return $this->redirectToRoute("home");
    }
}
