<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoursRepository")
 */
class Cours
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Exercice", mappedBy="cours")
     */
    private $exercices;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="cours")
     */
    private $etudiants;

    public function __construct()
    {
        $this->exercices = new ArrayCollection();
        $this->etudiants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Exercice[]
     */
    public function getExercices(): Collection
    {
        return $this->exercices;
    }

    public function addExercice(Exercice $exercice): self
    {
        if (!$this->exercices->contains($exercice)) {
            $this->exercices[] = $exercice;
            $exercice->setCours($this);
        }

        return $this;
    }

    public function removeExercice(Exercice $exercice): self
    {
        if ($this->exercices->contains($exercice)) {
            $this->exercices->removeElement($exercice);
            // set the owning side to null (unless already changed)
            if ($exercice->getCours() === $this) {
                $exercice->setCours(null);
            }
        }

        return $this;
    }

    public function countExercices() {
        return sizeof($this->exercices);
    }

    public function countEtudiants() {
        return sizeof($this->etudiants);
    }

    public function countSuccess() {
        $total = 0;
        foreach ($this->exercices as $exercice) {
            $total += $exercice->countSuccess();
        }
        return $total;
    }

    public function countFail() {
        $total = 0;
        foreach ($this->exercices as $exercice) {
            $total += $exercice->countFail();
        }
        return $total;
    }

    /*public function getEtudiants() {
        $etudiants = [];
        foreach ($this->exercices as $exercice) {
            $e = $exercice->getEtudiants();
            foreach ($e as $etudiant) {
                if(in_array($etudiant, $etudiants)) {
                }
                else {
                    $etudiants[] = $etudiant;
                }
                $etudiants[] = $etudiant;
            }
        }
        return $etudiants;
    }*/

    /**
     * @return Collection|User[]
     */
    public function getEtudiants(): Collection
    {
        return $this->etudiants;
    }

    public function addEtudiant(User $etudiant): self
    {
        if (!$this->etudiants->contains($etudiant)) {
            $this->etudiants[] = $etudiant;
        }

        return $this;
    }

    public function removeEtudiant(User $etudiant): self
    {
        if ($this->etudiants->contains($etudiant)) {
            $this->etudiants->removeElement($etudiant);
        }

        return $this;
    }
}

