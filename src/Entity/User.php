<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{

    public const USER_TYPE_ETUDIANT = 1;
    public const USER_TYPE_ENSEIGNANT = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mdp;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Resultat", mappedBy="etudiant", orphanRemoval=true)
     */
    private $resultats;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Cours", mappedBy="etudiants")
     */
    private $cours;

    public function __construct()
    {
        $this->resultats = new ArrayCollection();
        $this->cours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getMdp(): ?string
    {
        return $this->mdp;
    }

    public function setMdp(string $mdp): self
    {
        $this->mdp = $mdp;

        return $this;
    }

    /**
     * @return Collection|Resultat[]
     */
    public function getResultats(): Collection
    {
        return $this->resultats;
    }

    public function addResultat(Resultat $resultat): self
    {
        if (!$this->resultats->contains($resultat)) {
            $this->resultats[] = $resultat;
            $resultat->setEtudiant($this);
        }

        return $this;
    }

    public function removeResultat(Resultat $resultat): self
    {
        if ($this->resultats->contains($resultat)) {
            $this->resultats->removeElement($resultat);
            // set the owning side to null (unless already changed)
            if ($resultat->getEtudiant() === $this) {
                $resultat->setEtudiant(null);
            }
        }

        return $this;
    }

    public function countSuccess() {
        $total = 0;
        foreach ($this->resultats as $resultat) {
            if($resultat->getResultat() >= 50) {
                $total += 1;
            }
        }
        return $total;
    }

    public function countFail() {
        $total = 0;
        foreach ($this->resultats as $resultat) {
            if($resultat->getResultat() < 50) {
                $total += 1;
            }
        }
        return $total;
    }

    public function getExercices() {
        $exercices = [];
        foreach ($this->resultats as $resultat) {
            $exercices[] = $resultat->getExercice();
        }
        return $exercices;
    }

    public function getCours() {
        return $this->cours;
    }

    public function addCour(Cours $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
            $cour->addEtudiant($this);
        }

        return $this;
    }

    public function removeCour(Cours $cour): self
    {
        if ($this->cours->contains($cour)) {
            $this->cours->removeElement($cour);
            $cour->removeEtudiant($this);
        }

        return $this;
    }
}
