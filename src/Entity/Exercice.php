<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExerciceRepository")
 */
class Exercice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cours", inversedBy="exercices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cours;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ligne", mappedBy="exercice", orphanRemoval=true)
     */
    private $lignes;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Resultat", mappedBy="exercice", orphanRemoval=true)
     */
    private $resultats;

    public function __construct()
    {
        $this->lignes = new ArrayCollection();
        $this->solutions = new ArrayCollection();
        $this->resultats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCours(): ?Cours
    {
        return $this->cours;
    }

    public function setCours(?Cours $cours): self
    {
        $this->cours = $cours;

        return $this;
    }

    /**
     * @return Collection|Ligne[]
     */
    public function getLignes(): Collection
    {
        return $this->lignes;
    }

    public function addLigne(Ligne $ligne): self
    {
        if (!$this->lignes->contains($ligne)) {
            $this->lignes[] = $ligne;
            $ligne->setExercice($this);
        }

        return $this;
    }

    public function removeLigne(Ligne $ligne): self
    {
        if ($this->lignes->contains($ligne)) {
            $this->lignes->removeElement($ligne);
            // set the owning side to null (unless already changed)
            if ($ligne->getExercice() === $this) {
                $ligne->setExercice(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Resultat[]
     */
    public function getResultats(): Collection
    {
        return $this->resultats;
    }

    public function addResultat(Resultat $resultat): self
    {
        if (!$this->resultats->contains($resultat)) {
            $this->resultats[] = $resultat;
            $resultat->setExercice($this);
        }

        return $this;
    }

    public function removeResultat(Resultat $resultat): self
    {
        if ($this->resultats->contains($resultat)) {
            $this->resultats->removeElement($resultat);
            // set the owning side to null (unless already changed)
            if ($resultat->getExercice() === $this) {
                $resultat->setExercice(null);
            }
        }

        return $this;
    }

    public function countResultats() {
        return sizeof($this->resultats);
    }

    public function countSuccess() {
        $total = 0;
        foreach ($this->resultats as $resultat) {
            if($resultat->getResultat() > 0) {
                $total += 1;
            }
        }
        return $total;
    }

    public function countFail() {
        $total = 0;
        foreach ($this->resultats as $resultat) {
            if($resultat->getResultat() == 0) {
                $total += 1;
            }
        }
        return $total;
    }

    public function countSuccessByUser($user) {
        $total = 0;
        foreach ($this->resultats as $resultat) {
            if($resultat->getResultat() > 0 && $resultat->getEtudiant() == $user) {
                $total += 1;
            }
        }
        return $total;
    }

    public function countFailByUser($user) {
        $total = 0;
        foreach ($this->resultats as $resultat) {
            if($resultat->getResultat() == 0 && $resultat->getEtudiant() == $user) {
                $total += 1;
            }
        }
        return $total;
    }

    public function getEtudiants() {
        $etudiants = [];
        foreach ($this->resultats as $resultat) {
            $etudiant[] = $resultat->getEtudiant();
        }
        return $etudiants;
    }
}
