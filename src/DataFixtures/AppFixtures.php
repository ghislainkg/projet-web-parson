<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Cours;
use App\Entity\Exercice;
use App\Entity\User;
use App\Entity\Ligne;
use App\Entity\Resultat;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        /*$etudiants = [];
        for ($i=0; $i < 5; $i++) { 
            $etudiant = new User();
            $etudiant->setType(User::USER_TYPE_ETUDIANT)
                ->setNom("Etudiant ".$i)
                ->setMdp("mdp ".$i);
            $etudiants[] = $etudiant;
            $manager->persist($etudiant);
        }
        
        for ($i=0; $i < 4; $i++) {

            // Creation d'un cours
            $cours = new Cours();
            $cours->setNom("Cours ".$i)
                ->setDescription("Lorem ipsum dolor sit, amet consectetur adipisicing elit. Neque illo ipsam eum illum recusandae similique reprehenderit aperiam voluptates, officiis, possimus rem, modi unde rerum expedita nemo voluptate. Minus, qui eos!")
                ;

            for ($j=0; $j < 2; $j++) { 
                // Creation d'un exercice
                $exercice = new Exercice();
                $exercice->setNom("Exercice ".$j)
                    ->setDescription("Lorem, ipsum dolor sit amet consectetur adipisicing elit. Similique ratione perspiciatis quas, ducimus officiis harum, recusandae a nesciunt aliquam voluptatum placeat architecto vitae eaque velit reprehenderit laboriosam assumenda! Eaque, dolore!")
                    ->setCours($cours);

                $manager->persist($exercice);

                // creation du resultat
                $resultat = new Resultat();
                $resultat->setExercice($exercice)
                    ->setEtudiant($etudiants[rand(0, 4)])
                    ->setResultat(20);
                $manager->persist($resultat);

                for ($k=0; $k < 4; $k++) { 
                    // Creation d'une ligne
                    $ligne = new Ligne();
                    $ligne->setContenu("Ligne ligne ligne ".$k)
                        ->setExercice($exercice)
                        ->setNumero($k);
                    $manager->persist($ligne);
                }
            }

            $manager->persist($cours);
        }

        $manager->flush();*/
    }
}
